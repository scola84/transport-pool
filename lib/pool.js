'use strict';

const EventHandler = require('@scola/events');

class AbstractPool extends EventHandler {
  constructor() {
    super();

    this.transport = null;
    this.connections = new Set();
  }

  open(transport) {
    this.emit('debug', this, 'open', transport);

    this.transport = transport;
    this.bindTransportListeners();

    return this;
  }

  close() {
    this.emit('debug', this, 'close');
    this.unbindTransportListeners();

    return this;
  }

  send(message) {
    this.emit('debug', this, 'send', message);
    return this;
  }

  cancel(message) {
    this.emit('debug', this, 'cancel', message);
    return this;
  }

  createMessage() {
    this.emit('debug', this, 'createMessage');
    return this.transport.createMessage();
  }

  bindTransportListeners() {
    this.bindListener('connection', this.transport, this.handleConnection);
  }

  unbindTransportListeners() {
    this.unbindListener('connection', this.transport, this.handleConnection);
  }

  bindConnectionListeners(connection) {
    this.bindListener('close', connection, this.handleClose);
    this.proxyListener('debug', connection);
    this.proxyListener('error', connection);
    this.proxyListener('message', connection);
    this.proxyListener('open', connection);
  }

  unbindConnectionListeners(connection) {
    this.unbindListener('close', connection, this.handleClose);
    this.unproxyListener('debug', connection);
    this.unproxyListener('error', connection);
    this.unproxyListener('message', connection);
    this.unproxyListener('open', connection);
  }

  handleConnection(connection) {
    this.connections.add(connection);
    this.bindConnectionListeners(connection);
    this.emit('connection', connection);
  }

  handleClose(connection) {
    this.connections.delete(connection);
    this.unbindConnectionListeners(connection);
    this.emit('close', connection);
  }
}

module.exports = AbstractPool;
